const ratesRequested = () => ({
  type: 'FETCH_RATES_REQUEST',
});

const ratesLoaded = rates => ({
  type: 'FETCH_RATES_SUCCESS',
  payload: rates,
});

const ratesError = error => ({
  type: 'FETCH_RATES_ERROR',
  payload: error,
});

const changeBaseCurrency = currencyCode => ({
  type: 'SETTINGS_CHANGE_CURRENCY',
  payload: currencyCode,
});

const setLocale = locale => ({
  type: 'SWITCH_LOCALE',
  payload: locale,
});

const setFavorite = favorite => ({
  type: 'FAVORITE_SET',
  payload: favorite,
});

const addToFavorite = currencyCode => ({
  type: 'FAVORITE_ADD',
  payload: currencyCode,
});

const removeFromFavorite = currencyCode => ({
  type: 'FAVORITE_REMOVE',
  payload: currencyCode,
});

const fetchRates = (ratesApiService, dispatch) => async () => {
  dispatch(ratesRequested());

  try {
    const rates = await ratesApiService.getExchangeRates();
    dispatch(ratesLoaded(rates));
  } catch (err) {
    dispatch(ratesError(err));
  }
};

export {
  ratesRequested,
  ratesLoaded,
  ratesError,
  fetchRates,
  changeBaseCurrency,
  setLocale,
  addToFavorite,
  removeFromFavorite,
  setFavorite,
};
