import React from 'react';
import {StyleSheet} from 'react-native';
import {Provider} from 'react-redux';

import RootContexts from './RootContexts';
import Router from '../navigation/Router';
import store from '../store';
import init from '../bootstrap';
import {Surface} from '../components/base';

class App extends React.PureComponent {
  state = {
    isBootDone: false,
  };

  async componentDidMount() {
    await init();
    this.setState({isBootDone: true});
  }

  render() {
    const {isBootDone} = this.state;

    return (
      <Provider store={store}>
        {isBootDone && (
          <RootContexts>
            <Surface style={styles.container} color="background">
              <Router />
            </Surface>
          </RootContexts>
        )}
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
});

export default App;
