import React from 'react';
import {connect} from 'react-redux';
import {ExchangeRatesApiServiceContext} from '../../contexts';
import {ExchangeRatesApiService} from '../../core';

class ExchangeRatesApiServiceRootContext extends React.PureComponent {
  state = {
    ratesApiService: new ExchangeRatesApiService(this.props.baseCurrency),
  };

  componentDidUpdate(prevProps) {
    if (prevProps.baseCurrency !== this.props.baseCurrency) {
      this.handleChangeBaseCurrency();
    }
  }

  handleChangeBaseCurrency() {
    this.state.ratesApiService.setBaseCurrency(this.props.baseCurrency);
  }

  render() {
    return (
      <ExchangeRatesApiServiceContext.Provider
        value={this.state.ratesApiService}>
        {this.props.children}
      </ExchangeRatesApiServiceContext.Provider>
    );
  }
}

const mapStateToProps = ({settings: {baseCurrency}}) => ({
  baseCurrency,
});

export default connect(mapStateToProps)(ExchangeRatesApiServiceRootContext);
