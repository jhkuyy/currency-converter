import React from 'react';
import {ThemeContext} from '../../contexts';
import DeviceStorage from '../../modules/DeviceStorage';
import Theme from '../../theme';

class ThemeRootContext extends React.PureComponent {
  static initialTheme = null;

  static setInitialTheme(theme) {
    ThemeRootContext.initialTheme = theme;
  }

  constructor(props) {
    super(props);

    this.state = {
      theme: ThemeRootContext.initialTheme,
    };
  }

  setTheme = theme => {
    this.setState({theme});
    DeviceStorage.setItem(DeviceStorage.Keys.THEME, theme);
  };

  render() {
    const {
      setTheme,
      state: {theme},
    } = this;

    return (
      <ThemeContext.Provider value={{theme, colors: Theme[theme], setTheme}}>
        {this.props.children}
      </ThemeContext.Provider>
    );
  }
}

export default ThemeRootContext;
