import React from 'react';
import ExchangeRatesApiServiceRootContext from './ExchangeRatesApiService';
import ThemeRootContext from './Theme';

export default class RootContexts extends React.PureComponent {
  render() {
    return (
      <ThemeRootContext>
        <ExchangeRatesApiServiceRootContext>
          {this.props.children}
        </ExchangeRatesApiServiceRootContext>
      </ThemeRootContext>
    );
  }
}
