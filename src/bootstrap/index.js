import SplashScreen from 'react-native-splash-screen';

import DeviceStorage from '../modules/DeviceStorage';
import Store from '../store';
import {changeBaseCurrency, setFavorite, setLocale} from '../actions';
import ThemeRootContext from '../app/RootContexts/Theme';
import {ThemeConfig} from '../config';

const init = async () => {
  const [
    [, language],
    [, currency],
    [, favorite],
    [, theme],
  ] = await DeviceStorage.multiGet([
    DeviceStorage.Keys.APP_SETTINGS_LANGUAGE,
    DeviceStorage.Keys.BASE_CURRENCY,
    DeviceStorage.Keys.FAVORITE,
    DeviceStorage.Keys.THEME,
  ]);

  Store.dispatch(changeBaseCurrency(currency || 'USD'));
  Store.dispatch(setLocale(language || 'en'));
  Store.dispatch(setFavorite(JSON.parse(favorite) || []));
  ThemeRootContext.setInitialTheme(theme || ThemeConfig.DEFAULT_THEME);

  setTimeout(() => {
    SplashScreen.hide();
  }, 500);
};

export default init;
