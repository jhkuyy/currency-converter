import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, TouchableOpacity, View} from 'react-native';

import Text from './Text';
import ThemeColor from '../../theme/ThemeColor';

const propTypes = {
  color: PropTypes.string,
  textColor: PropTypes.string,
  touchAvailable: PropTypes.bool,
};

const defaultProps = {
  color: null,
  textColor: ThemeColor.TEXT_PRIMARY,
  loading: false,
};

class Button extends React.PureComponent {
  get touchAvailable() {
    const {disabled, loading} = this.props;

    return !disabled && !loading;
  }

  render() {
    const {
      touchAvailable,
      props: {children, style, textColor, ...props},
    } = this;

    return (
      <TouchableOpacity {...props} style={style} disabled={!touchAvailable}>
        <View style={styles.container}>
          <View style={styles.contentRow}>
            {typeof children === 'string' ? (
              <Text color={textColor}>{children}</Text>
            ) : (
              children
            )}
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

Button.propTypes = propTypes;
Button.defaultProps = defaultProps;

const styles = StyleSheet.create({
  container: {height: 50},
  contentRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
    flex: 1,
  },
});

export default Button;
