import React from 'react';
import PropTypes from 'prop-types';
import {View} from 'react-native';
import ThemeColor from '../../theme/ThemeColor';
import {ThemeContext} from '../../contexts';

const propTypes = {
  color: PropTypes.oneOf([
    'background',
    'background:secondary',
    'input:background',
  ]),
};

const defaultProps = {};

const ThemeColorMap = {
  separator: ThemeColor.SEPARATOR,
  background: ThemeColor.BACKGROUND,
  'background:secondary': ThemeColor.BACKGROUND_SECONDARY,
  'input:background': ThemeColor.INPUT_TEXT_BACKGROUND,
};

const Surface = ({children, style, color, ...props}) => {
  const {colors} = React.useContext(ThemeContext);
  const backgroundColor = colors[ThemeColorMap[color]];

  return (
    <View {...props} style={[{backgroundColor}, style]}>
      {children}
    </View>
  );
};

Surface.propTypes = propTypes;
Surface.defaultProps = defaultProps;

export default Surface;
