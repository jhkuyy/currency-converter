import React from 'react';
import PropTypes from 'prop-types';
import {Text as BaseText} from 'react-native';

import {fontStyleStyles} from './textAppearance';
import TextLocalizedString from './TextLocalizedString';
import {ThemeContext} from '../../../contexts';
import ThemeColor from '../../../theme/ThemeColor';

const propTypes = {
  fontStyle: PropTypes.string,
  color: PropTypes.string,
  align: PropTypes.string,
};

const defaultProps = {
  fontStyle: null,
  color: 'primary',
  align: 'left',
};

const ThemeColorMapping = {
  primary: ThemeColor.TEXT_PRIMARY,
  secondary: ThemeColor.TEXT_SECONDARY,
  'primary:inactive': ThemeColor.TEXT_PRIMARY_INACTIVE,
};

class Text extends React.PureComponent {
  static contextType = ThemeContext;

  render() {
    const {
      context: {colors},
      props: {children, fontStyle, style, color, align, ...props},
    } = this;

    return (
      <BaseText
        {...props}
        style={[
          {textAlign: align},
          fontStyle && fontStyleStyles[fontStyle],
          style,
          {color: colors[ThemeColorMapping[color]]},
        ]}>
        {children}
      </BaseText>
    );
  }
}

Text.propTypes = propTypes;
Text.defaultProps = defaultProps;
Text.LocalizedString = TextLocalizedString;

export default Text;
