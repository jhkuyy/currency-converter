import React from 'react';
import {connect} from 'react-redux';

import PropTypes from 'prop-types';

const propTypes = {
  dictionary: PropTypes.object.isRequired,
};

class TextLocalizedString extends React.PureComponent {
  render() {
    return this.props.dictionary[this.props.children] || '';
  }
}

TextLocalizedString.propTypes = propTypes;

const mapStateToProps = ({locale: {dictionary}}) => ({
  dictionary,
});

export default connect(mapStateToProps)(TextLocalizedString);
