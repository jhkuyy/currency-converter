import {StyleSheet} from 'react-native';

export const FontStyles = {
  Mono: {
    BOLD: 'mono:bold',
    HEAVY: 'mono:heavy',
    REGULAR: 'mono:regular',
  },
};

export const FontStylesFontFamily = {
  Mono: {
    BOLD: 'SFMono-Bold',
    HEAVY: 'SFMono-Heavy',
    REGULAR: 'SFMono-Regular',
  },
};

export const fontStyleStyles = StyleSheet.create({
  [FontStyles.Mono.BOLD]: {fontFamily: FontStylesFontFamily.Mono.BOLD},
  [FontStyles.Mono.HEAVY]: {fontFamily: FontStylesFontFamily.Mono.HEAVY},
  [FontStyles.Mono.REGULAR]: {fontFamily: FontStylesFontFamily.Mono.REGULAR},
});
