import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, View} from 'react-native';

import {Text} from '../base';
import Currency from '../../resource/Currency';
import FavoriteIcon from './FavoriteIcon';

const propTypes = {
  code: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  isFavorite: PropTypes.bool,
  bgColor: PropTypes.string,
};

const defaultProps = {
  bgColor: 'transparent',
  isFavorite: false,
};

class CurrencyItem extends React.PureComponent {
  get barStyle() {
    return {
      backgroundColor: this.props.bgColor,
    };
  }

  render() {
    const {
      barStyle,
      props: {children, code, name, isFavorite},
    } = this;

    return (
      <View style={[styles.bar, barStyle]}>
        {isFavorite && <FavoriteIcon style={styles.star} size={10} isActive />}
        <View>
          <View style={styles.row}>
            <Text fontStyle="mono:bold" style={styles.currencyCode}>
              {code}
            </Text>
            <Text
              style={styles.currencySymbol}
              fontStyle="mono:regular"
              color="secondary">
              {Currency[code].symbol_native}
            </Text>
          </View>

          <Text>
            <Text.LocalizedString style={styles.currencyName}>
              {name}
            </Text.LocalizedString>
          </Text>
        </View>
        <View>{children}</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  bar: {
    padding: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  currencyCode: {
    fontSize: 28,
  },
  currencyName: {
    fontSize: 16,
  },
  currencySymbol: {
    fontSize: 28,
    marginLeft: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  star: {
    position: 'absolute',
    right: 15,
    top: 15,
  },
});

CurrencyItem.propTypes = propTypes;
CurrencyItem.defaultProps = defaultProps;

export default CurrencyItem;
