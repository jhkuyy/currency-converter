import React from 'react';
import {faStar} from '@fortawesome/free-solid-svg-icons';
import ThemeColor from '../../theme/ThemeColor';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {ThemeContext} from '../../contexts';

const FavoriteIcon = ({size, isActive, style}) => {
  const {colors} = React.useContext(ThemeContext);

  return (
    <FontAwesomeIcon
      style={style}
      icon={faStar}
      size={size}
      color={
        isActive
          ? colors[ThemeColor.FAVORITE]
          : colors[ThemeColor.TEXT_PRIMARY_INACTIVE]
      }
    />
  );
};

export default FavoriteIcon;
