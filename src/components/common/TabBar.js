import React from 'react';
import {StyleSheet, View, TouchableOpacity} from 'react-native';

import {Text} from '../base';
import DictionaryMapping from '../../i18n/DictionaryMapping';
import ThemeColor from '../../theme/ThemeColor';
import {ThemeContext} from '../../contexts';

const routesMap = {
  Converter: DictionaryMapping.Tabs.CONVERTER,
  Settings: DictionaryMapping.Tabs.SETTINGS,
};

class TabBar extends React.PureComponent {
  static contextType = ThemeContext;

  render() {
    const {
      context: {colors},
      props: {
        renderIcon,
        onTabPress,
        onTabLongPress,
        getAccessibilityLabel,
        navigation,
      },
    } = this;

    const {routes, index: activeRouteIndex} = navigation.state;

    return (
      <View style={styles.container}>
        {routes.map((route, routeIndex) => {
          const isRouteActive = routeIndex === activeRouteIndex;
          const tintColor = isRouteActive
            ? colors[ThemeColor.TEXT_PRIMARY]
            : colors[ThemeColor.TEXT_PRIMARY_INACTIVE];
          const textColor = isRouteActive ? 'primary' : 'primary:inactive';

          return (
            <View key={routeIndex} style={[styles.tabButton]}>
              <TouchableOpacity
                style={styles.tabButton}
                onPress={() => onTabPress({route})}
                onLongPress={() => onTabLongPress({route})}
                accessibilityLabel={getAccessibilityLabel({route})}>
                <View style={styles.tabButtonInner}>
                  {renderIcon({route, focused: isRouteActive, tintColor})}
                  <Text style={styles.tabButtonText} color={textColor}>
                    <Text.LocalizedString style={styles.currencyName}>
                      {DictionaryMapping.Tabs[routesMap[route.routeName]]}
                    </Text.LocalizedString>
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          );
        })}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 52,
    elevation: 2,
    alignItems: 'stretch',
    width: '100%',
  },
  tabButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  tabButtonInner: {
    alignItems: 'center',
  },
  tabButtonText: {
    fontSize: 9,
    marginTop: 5,
  },
});

export default TabBar;
