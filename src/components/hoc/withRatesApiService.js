import React from 'react';

import {ExchangeRatesApiServiceContext} from '../../contexts';

const withRatesApiServiceService = mapMethodsToProps => Wrapped => {
  return props => {
    return (
      <ExchangeRatesApiServiceContext.Consumer>
        {ratesApiService => {
          const serviceProps = mapMethodsToProps(ratesApiService);

          return <Wrapped {...props} {...serviceProps} />;
        }}
      </ExchangeRatesApiServiceContext.Consumer>
    );
  };
};

export default withRatesApiServiceService;
