import React from 'react';
import {compose} from 'redux';
import {connect} from 'react-redux';
import {StyleSheet, View, FlatList} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faAngleDown, faSpinner} from '@fortawesome/free-solid-svg-icons';

import ConverterTextInput from './ConverterTextInput';
import {CurrencyItem} from '../../common';
import {Button, Surface, Text} from '../../base';
import withRatesApiServiceService from '../../hoc/withRatesApiService';
import Currency from '../../../resource/Currency';
import {fetchRates} from '../../../actions';
import Routes from '../../../navigation/routes';
import DictionaryMapping from '../../../i18n/DictionaryMapping';
import I18n from '../../../i18n';

class Converter extends React.PureComponent {
  state = {
    value: '',
    baseCurrency: null,
  };

  constructor(props) {
    super(props);
    this.isFavorite = this.isFavorite.bind(this);
    this.update = this.update.bind(this);
  }

  async update() {
    await this.props.fetchRates();
    this.updateBaseCurrency();
  }

  async componentDidMount() {
    this.update();
  }

  async componentDidUpdate(prevProps) {
    if (prevProps.baseCurrency !== this.props.baseCurrency) {
      await this.props.fetchRates();
      this.updateBaseCurrency();
    }
  }

  onChangeValue = value => {
    this.setState({
      value: !['.', ''].includes(value) ? value.replace(/[^.+\d]/g, '') : '',
    });
  };

  updateBaseCurrency() {
    if (!(this.props.loading || this.props.rates.length === 0)) {
      this.setState({
        baseCurrency: this.props.rates.find(
          item => item.code === this.props.baseCurrency,
        ),
      });
    }
  }

  get filterRates() {
    const filteredRates = this.props.rates.filter(
      item => item.code !== this.props.baseCurrency,
    );

    filteredRates.sort((a, b) => {
      const isAFavorite = this.isFavorite(a.code);
      const isBFavorite = this.isFavorite(b.code);

      if (isAFavorite && !isBFavorite) {
        return -1;
      }
      if (isBFavorite && !isAFavorite) {
        return 1;
      }

      return 0;
    });

    return filteredRates;
  }

  isFavorite(code) {
    return this.props.favorite.includes(code);
  }

  renderCurrencyItem = ({item: {code, value}}) => {
    const rate = this.state.value * value;
    const digits = Currency[code].decimal_digits;

    return (
      <CurrencyItem
        name={DictionaryMapping.Currencies[code]}
        code={code}
        isFavorite={this.isFavorite(code)}>
        {this.state.value !== '' && (
          <Text fontStyle="mono:regular">{rate.toFixed(digits)}</Text>
        )}
      </CurrencyItem>
    );
  };

  render() {
    const {
      renderCurrencyItem,
      onChangeValue,
      filterRates,
      update,
      state: {value},
      props: {navigation, loading, baseCurrency, locale, error},
    } = this;

    return (
      <Surface style={styles.container} color="background">
        <ConverterTextInput
          placeholder={I18n.getTranslationByDictionaryKey(
            locale.lang,
            DictionaryMapping.Ui.ENTER_VALUE,
          )}
          value={value}
          onChange={onChangeValue}
        />
        <Surface style={styles.baseCurrencyWrap} color="background:secondary">
          <CurrencyItem
            name={DictionaryMapping.Currencies[baseCurrency]}
            code={baseCurrency}>
            <Button onPress={() => navigation.push(Routes.CURRENCIES)}>
              <FontAwesomeIcon icon={faAngleDown} c />
            </Button>
          </CurrencyItem>
        </Surface>
        {loading && (
          <View style={styles.spinner}>
            <FontAwesomeIcon icon={faSpinner} size={30} spin />
          </View>
        )}
        {!(loading || !baseCurrency) && (
          <FlatList
            keyExtractor={item => item.code}
            data={filterRates}
            renderItem={renderCurrencyItem}
            ItemSeparatorComponent={() => (
              <Surface style={styles.separator} color="separator" />
            )}
          />
        )}
        {error && (
          <>
            <Text align="center">
              <Text.LocalizedString>
                {DictionaryMapping.Ui.LOADING_ERROR}
              </Text.LocalizedString>
            </Text>
            <Button onPress={update}>
              <Text>
                <Text.LocalizedString>
                  {DictionaryMapping.Ui.TRY_AGAIN}
                </Text.LocalizedString>
              </Text>
            </Button>
          </>
        )}
      </Surface>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  spinner: {
    padding: 25,
    alignItems: 'center',
  },
  baseCurrencyWrap: {
    elevation: 5,
  },
  separator: {
    height: StyleSheet.hairlineWidth,
  },
});

const mapStateToProps = ({
  locale,
  ratesList: {rates, loading, error},
  settings: {baseCurrency},
  favorite,
}) => ({
  rates,
  loading,
  error,
  baseCurrency,
  locale,
  favorite: favorite.items,
});

const mapDispatchToProps = (dispatch, {ratesApiService}) => {
  return {
    fetchRates: fetchRates(ratesApiService, dispatch),
  };
};

const mapMethodsToProps = ratesApiService => {
  return {ratesApiService};
};

export default compose(
  withRatesApiServiceService(mapMethodsToProps),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(Converter);
