import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, TextInput} from 'react-native';

import {FontStylesFontFamily} from '../../base/Text/textAppearance';
import {ThemeContext} from '../../../contexts';
import ThemeColor from '../../../theme/ThemeColor';

const propTypes = {
  value: PropTypes.string,
};

class ConverterTextInput extends React.PureComponent {
  static contextType = ThemeContext;

  render() {
    const {
      context: {colors},
      props: {value, onChange, placeholder},
    } = this;

    return (
      <TextInput
        style={[
          styles.input,
          {backgroundColor: colors[ThemeColor.INPUT_TEXT_BACKGROUND]},
          {color: colors[ThemeColor.TEXT_PRIMARY]},
        ]}
        onChangeText={text => onChange(text)}
        placeholder={placeholder}
        keyboardType="numeric"
        value={value}
        placeholderColor={colors[ThemeColor.TEXT_PRIMARY_INACTIVE]}
      />
    );
  }
}

const styles = StyleSheet.create({
  input: {
    fontSize: 40,
    lineHeight: 40,
    fontFamily: FontStylesFontFamily.Mono.REGULAR,
    paddingHorizontal: 15,
    paddingVertical: 25,
    borderBottomWidth: 1,
  },
});

ConverterTextInput.propTypes = propTypes;

export default ConverterTextInput;
