import React from 'react';
import {StyleSheet, FlatList, View, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCheck} from '@fortawesome/free-solid-svg-icons';

import {Surface, Text} from '../base';
import FavoriteIcon from '../common/FavoriteIcon';
import CurrencyItem from '../common/CurrencyItem';
import {
  changeBaseCurrency,
  addToFavorite,
  removeFromFavorite,
} from '../../actions';
import DictionaryMapping from '../../i18n/DictionaryMapping';

class Currencies extends React.PureComponent {
  static navigationOptions = () => ({
    headerTitle: () => (
      <Text appearance="header">
        <Text.LocalizedString>
          {DictionaryMapping.ScreenTitle.CURRENCIES}
        </Text.LocalizedString>
      </Text>
    ),
  });

  constructor(props) {
    super(props);
    this.changeLocale = this.changeLocale.bind(this);
    this.toggleFavorite = this.toggleFavorite.bind(this);
    this.isFavorite = this.isFavorite.bind(this);
    this.checkIconStyles = this.checkIconStyles.bind(this);
    this.renderItem = this.renderItem.bind(this);
  }

  toggleFavorite(code) {
    !this.isFavorite(code)
      ? this.props.addToFavorite(code)
      : this.props.removeFromFavorite(code);
  }

  changeLocale(locale) {
    this.props.onBaseCurrencyChange(locale);
    this.props.navigation.pop();
  }

  checkIconStyles(code) {
    return {
      color:
        this.props.baseCurrency === code ? 'green' : 'rgba(155, 155, 155, .3)',
    };
  }

  isFavorite(code) {
    return this.props.favorite.includes(code);
  }

  renderItem({item: {code}}) {
    const {changeLocale, toggleFavorite, checkIconStyles, isFavorite} = this;

    return (
      <CurrencyItem
        key={code}
        appearance="list"
        code={code}
        name={DictionaryMapping.Currencies[code]}>
        <View style={styles.row}>
          <TouchableOpacity
            onPress={() => toggleFavorite(code)}
            style={styles.button}>
            <FavoriteIcon size={20} isActive={isFavorite(code)} />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => changeLocale(code)}>
            <FontAwesomeIcon
              style={checkIconStyles(code)}
              icon={faCheck}
              size={20}
            />
          </TouchableOpacity>
        </View>
      </CurrencyItem>
    );
  }

  render() {
    const {
      renderItem,
      props: {rates},
    } = this;

    return (
      <Surface color="background">
        <FlatList
          keyExtractor={item => item.code}
          data={rates}
          renderItem={renderItem}
          ItemSeparatorComponent={() => (
            <Surface style={styles.separator} color="separator" />
          )}
        />
      </Surface>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  button: {
    padding: 15,
    marginLeft: 5,
  },
  separator: {
    height: StyleSheet.hairlineWidth,
  },
});

const mapStateToProps = ({
  locale,
  ratesList: {rates},
  settings: {baseCurrency},
  favorite: {items},
}) => ({
  rates,
  baseCurrency,
  locale,
  favorite: items,
});

const mapDispatchToProps = {
  onBaseCurrencyChange: changeBaseCurrency,
  addToFavorite,
  removeFromFavorite,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Currencies);
