import React from 'react';
import {View, Picker, StyleSheet} from 'react-native';
import {connect} from 'react-redux';

import {Text} from '../base';
import DictionaryMapping from '../../i18n/DictionaryMapping';
import I18n from '../../i18n';
import {setLocale} from '../../actions';
import {ThemeContext} from '../../contexts';
import {Theme} from '../../theme';
import ThemeColor from '../../theme/ThemeColor';

class Settings extends React.PureComponent {
  static contextType = ThemeContext;

  render() {
    const {
      context: {theme, colors, setTheme},
      props: {locale, onSwitchLocale},
    } = this;

    const languagePickerData = [
      {
        code: 'en',
        label: I18n.getTranslationByDictionaryKey(
          locale.lang,
          DictionaryMapping.Languages.ENGLISH,
        ),
      },
      {
        code: 'ru',
        label: I18n.getTranslationByDictionaryKey(
          locale.lang,
          DictionaryMapping.Languages.RUSSIAN,
        ),
      },
    ];

    const themePickerData = [
      {
        theme: Theme.MATERIAL_LIGHT,
        label: I18n.getTranslationByDictionaryKey(
          locale.lang,
          DictionaryMapping.ColorTheme.LIGHT,
        ),
      },
      {
        theme: Theme.MATERIAL_PALENIGHT,
        label: I18n.getTranslationByDictionaryKey(
          locale.lang,
          DictionaryMapping.ColorTheme.DARK,
        ),
      },
    ];

    return (
      <View style={styles.container}>
        <Text>
          <Text.LocalizedString>
            {DictionaryMapping.Ui.CHOOSE_LANGUAGE}
          </Text.LocalizedString>
        </Text>
        <Picker
          selectedValue={locale.lang}
          style={[styles.picker, {color: colors[ThemeColor.TEXT_PRIMARY]}]}
          onValueChange={onSwitchLocale}>
          {languagePickerData.map(({code, label}) => (
            <Picker.Item key={code} label={label} value={code} />
          ))}
        </Picker>

        <Text>
          <Text.LocalizedString>
            {DictionaryMapping.Ui.CHOOSE_COLOR_THEME}
          </Text.LocalizedString>
        </Text>
        <Picker
          selectedValue={theme}
          style={[styles.picker, {color: colors[ThemeColor.TEXT_PRIMARY]}]}
          onValueChange={setTheme}>
          {themePickerData.map(({theme, label}) => (
            <Picker.Item key={theme} label={label} value={theme} />
          ))}
        </Picker>
      </View>
    );
  }
}

const mapStateToProps = ({locale}) => ({
  locale,
});

const mapDispatchToProps = {
  onSwitchLocale: setLocale,
};

const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
  picker: {
    height: 50,
    width: '100%',
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Settings);
