export default class ExchangeRatesApiService {
  baseUrl = 'https://api.exchangeratesapi.io';

  constructor(baseCurrency) {
    this.baseCurrency = baseCurrency;
  }

  setBaseCurrency(currency) {
    this.baseCurrency = currency;
  }

  async getExchangeRates() {
    const response = await fetch(
      `${this.baseUrl}/latest?base=${this.baseCurrency}`,
    ).then(response => response.json());

    return this._transformRates(response.rates);
  }

  _transformRates(rates) {
    return Object.entries(rates).map(([code, value]) => ({code, value}));
  }
}
