import Currencies from './Currencies';
import Ui from './Ui';
import Languages from './Languages';

export default {...Currencies, ...Ui, ...Languages};
