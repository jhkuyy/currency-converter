import AsyncStorage from '@react-native-community/async-storage';

const DeviceStorage = AsyncStorage;

DeviceStorage.Keys = {
  APP_SETTINGS_LANGUAGE: 'APP-SETTINGS:language',
  APP_SETTINGS_COLOR_THEME: 'APP-SETTINGS:color-theme',

  FAVORITE: 'FAVORITE',
  BASE_CURRENCY: 'BASE_CURRENCY',
  THEME: 'THEME',
};

export default DeviceStorage;
