import React from 'react';
import {createAppContainer} from 'react-navigation';

import TabsNavigator from './TabsNavigator';

const AppContainer = createAppContainer(TabsNavigator);

export default class Router extends React.PureComponent {
  render() {
    return <AppContainer />;
  }
}
