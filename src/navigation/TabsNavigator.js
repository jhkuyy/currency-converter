import React from 'react';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import {Converter, Currencies, Settings} from '../components/screen';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faExchangeAlt, faCog} from '@fortawesome/free-solid-svg-icons';

import Routes from './routes';
import {TabBar} from '../components/common';

const ConverterNavigator = createStackNavigator({
  [Routes.CONVERTER]: {
    screen: Converter,
    navigationOptions: {
      header: null,
    },
  },
  [Routes.CURRENCIES]: Currencies,
});

const TabNavigator = createBottomTabNavigator(
  {
    Converter: {
      screen: ConverterNavigator,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <FontAwesomeIcon icon={faExchangeAlt} color={tintColor} />
        ),
      },
    },
    Settings: {
      screen: Settings,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <FontAwesomeIcon icon={faCog} color={tintColor} />
        ),
      },
    },
  },
  {
    tabBarComponent: TabBar,
  },
);

export default TabNavigator;
