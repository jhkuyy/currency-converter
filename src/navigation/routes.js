export default {
  CONVERTER: 'converter',
  SETTINGS: 'settings',
  CURRENCIES: 'currencies',
};
