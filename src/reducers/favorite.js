import DeviceStorage from '../modules/DeviceStorage';

function setToDeviceStorage(items) {
  DeviceStorage.setItem(DeviceStorage.Keys.FAVORITE, JSON.stringify(items));
}

const updateFavorite = (state, action) => {
  if (state === undefined) {
    return {
      items: [],
    };
  }

  const items = state.favorite.items;

  switch (action.type) {
    case 'FAVORITE_SET':
      setToDeviceStorage(action.payload);
      return {
        items: action.payload,
      };
    case 'FAVORITE_ADD':
      if (items.includes(action.payload)) {
        return {...state.favorite};
      }
      setToDeviceStorage([...items, action.payload]);
      return {
        items: [...items, action.payload],
      };
    case 'FAVORITE_REMOVE':
      const index = items.findIndex(item => item === action.payload);

      if (index === -1) {
        return {...state.favorite};
      }

      const newItems = {
        items: [...items.slice(0, index), ...items.slice(index + 1)],
      };

      setToDeviceStorage(newItems.items);

      return newItems;
    default:
      return state.favorite;
  }
};

export default updateFavorite;
