import updateRatesList from './rates';
import updateSettings from './settings';
import updateLocale from './locale';
import updateFavorite from './favorite';

export default function reducer(state, action) {
  return {
    ratesList: updateRatesList(state, action),
    settings: updateSettings(state, action),
    locale: updateLocale(state, action),
    favorite: updateFavorite(state, action),
  };
}
