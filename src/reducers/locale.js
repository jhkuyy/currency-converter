import Dictionaries from '../i18n';
import DeviceStorage from '../modules/DeviceStorage';

const updateLocale = (state, action) => {
  if (state === undefined) {
    return {
      lang: null,
      dictionary: null,
    };
  }

  switch (action.type) {
    case 'SWITCH_LOCALE':
      DeviceStorage.setItem(
        DeviceStorage.Keys.APP_SETTINGS_LANGUAGE,
        action.payload,
      );
      return {
        lang: action.payload,
        dictionary: Dictionaries.getDictionaryByLocale(action.payload),
      };
    default:
      return state.locale;
  }
};

export default updateLocale;
