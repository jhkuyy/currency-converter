const updateRatesList = (state, action) => {
  if (state === undefined) {
    return {
      rates: [],
      loading: false,
      error: null,
    };
  }

  switch (action.type) {
    case 'FETCH_RATES_REQUEST':
      return {
        rates: state.ratesList.rates,
        loading: true,
        error: null,
      };
    case 'FETCH_RATES_SUCCESS':
      return {
        rates: action.payload,
        loading: false,
        error: null,
      };
    case 'FETCH_RATES_ERROR':
      return {
        rates: [],
        loading: false,
        error: action.payload,
      };
    default:
      return state.ratesList;
  }
};

export default updateRatesList;
