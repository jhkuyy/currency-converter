import DeviceStorage from '../modules/DeviceStorage';

const updateSettings = (state, action) => {
  if (state === undefined) {
    return {
      baseCurrency: null,
    };
  }

  switch (action.type) {
    case 'SETTINGS_CHANGE_CURRENCY':
      DeviceStorage.setItem(DeviceStorage.Keys.BASE_CURRENCY, action.payload);
      return {
        baseCurrency: action.payload,
      };
    default:
      return state.settings;
  }
};

export default updateSettings;
