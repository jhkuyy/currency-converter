import ThemeColor from './ThemeColor';

export default {
  [ThemeColor.BACKGROUND]: '#fff',
  [ThemeColor.BACKGROUND_SECONDARY]: '#fafafa',
  [ThemeColor.INPUT_TEXT_BACKGROUND]: '#fff',
  [ThemeColor.TEXT_PRIMARY]: '#222222',
  [ThemeColor.TEXT_PRIMARY_INACTIVE]: '#d3d9d9',
  [ThemeColor.TEXT_SECONDARY]: '#9ea7ac',
  [ThemeColor.SEPARATOR]: '#e0e2e1',
  [ThemeColor.FAVORITE]: 'rgb(251, 99, 71)',
};
