import ThemeColor from './ThemeColor';

export default {
  [ThemeColor.BACKGROUND]: '#292d3e',
  [ThemeColor.BACKGROUND_SECONDARY]: '#31364a',
  [ThemeColor.INPUT_TEXT_BACKGROUND]: '#222432',
  [ThemeColor.TEXT_PRIMARY]: '#eeffff',
  [ThemeColor.TEXT_PRIMARY_INACTIVE]: '#9faab2',
  [ThemeColor.TEXT_SECONDARY]: '#676d93',
  [ThemeColor.SEPARATOR]: '#353a52',
  [ThemeColor.FAVORITE]: 'rgb(251, 99, 71)',
};
