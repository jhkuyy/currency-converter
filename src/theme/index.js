import MaterialLight from './MaterialLight';
import MaterialPalenight from './MaterialPalenight';

export const Theme = {
  MATERIAL_LIGHT: 'MATERIAL_LIGHT',
  MATERIAL_PALENIGHT: 'MATERIAL_PALENIGHT',
};

export default {
  [Theme.MATERIAL_LIGHT]: MaterialLight,
  [Theme.MATERIAL_PALENIGHT]: MaterialPalenight,
};
